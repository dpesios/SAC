import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import sklearn.cluster
from sklearn.metrics import pairwise_distances
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN, HDBSCAN
from collections import Counter
from scipy.special import rel_entr, kl_div
from numpy import inf
from datetime import datetime, timedelta
from tabulate import tabulate
from random import sample
from sklearn.decomposition import PCA

import plotly as py
import plotly.graph_objs as go

pd.set_option('display.max_columns', None)

filename_list = ['./data/dsc_fc_summed_spectra_2016_v01.csv', './data/dsc_fc_summed_spectra_2017_v01.csv', \
                 './data/dsc_fc_summed_spectra_2018_v01.csv', './data/dsc_fc_summed_spectra_2019_v01.csv', \
                 './data/dsc_fc_summed_spectra_2020_v01.csv', './data/dsc_fc_summed_spectra_2021_v01.csv', \
                 './data/dsc_fc_summed_spectra_2022_v01.csv', './data/dsc_fc_summed_spectra_2023_v01.csv']

kp_file_name = './data/OMNI2_H0_MRG1HR_81346.csv1'

def loadDataset():
    dataframes_list = []
    for file_name in filename_list:
        file_dataDF = pd.read_csv(file_name, delimiter=',', parse_dates=[0], infer_datetime_format=True, na_values='0', header=None)
        dataframes_list.append(file_dataDF)

    datasetDF = pd.concat(dataframes_list)

    col_names = [str(i) for i in range(0, len(datasetDF.columns))]
    col_names[0] = 'Time'
    col_names[1] = 'B_x'
    col_names[2] = 'B_y'
    col_names[3] = 'B_z'

    datasetDF.columns = col_names

    # Convert timestamps to datetime indices
    datasetDF['Time'] = datasetDF['Time'].astype(str)
    datasetDF['Time'] = pd.to_datetime(datasetDF['Time'])
    datasetDF = datasetDF.set_index('Time')

    return datasetDF

def loadKpData():
    kp_dataDF = pd.read_csv(kp_file_name, delimiter=',', parse_dates=True, infer_datetime_format=True, na_values='0',
                                  skiprows=76, skipfooter=3, header="infer")

    # Properly format the timestamps
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].map(
        lambda x: x.split('.')[0].replace("T", " ")
    )

    # Convert timestamps to datetime indices
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].astype(str)
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = pd.to_datetime(kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'])

    # Reindex to be able to upsample the time series
    start_time = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].iloc[0]
    end_time = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].iloc[-1]
    t_index = pd.DatetimeIndex(pd.date_range(start=start_time-timedelta(seconds=30.0), end=end_time, freq="30S"))

    kp_dataDF = kp_dataDF.set_index('EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ')
    kp_dataDF = kp_dataDF.reindex(t_index)

    kp_dataDF = kp_dataDF.resample('30S', kind='timestamp').asfreq()[0:len(kp_dataDF)]

    # Fill NaN values with the average of adjacent values
    kp_dataDF['3-H_KP*10_'] = (kp_dataDF['3-H_KP*10_'].ffill() + kp_dataDF['3-H_KP*10_'].bfill()) / 2
    kp_dataDF['3-H_KP*10_'] = kp_dataDF['3-H_KP*10_'].bfill().ffill()

    kp_dataDF.index.name = 'Time'
    kp_dataDF = kp_dataDF.drop(['3-H_KP*10_CNTS', '3-H_KP*10_CNTS.1', '#_OF_3-H_KP*10_CNTS'], axis=1)

    return kp_dataDF

def importDatasetAsDF():
    datasetDF = loadDataset()
    kp_dataDF = loadKpData()

    overall_dataDF = pd.merge(datasetDF, kp_dataDF, on="Time")

    overall_per_3hour_dataDF = overall_dataDF.rolling(window=3 * 60, step=3 * 60).mean()

    return overall_per_3hour_dataDF


def main():
    specDF = importDatasetAsDF().drop(['B_x', 'B_y', 'B_z', '3-H_KP*10_'], axis=1).fillna(0)
    specDF = specDF.loc[(specDF.sum(axis=1) != 0)]
    specDF = specDF.sample(int(0.2*len(specDF)))

    # Drop columns that contain mainly NaNs
    drop_list = [str(i) for i in range(43, 54)]
    specDF = specDF.drop(columns=drop_list)

    dim = len(specDF)
    print(tabulate(specDF.head(20), headers='keys', tablefmt='psql'))
    print("There are " + str(dim) + " objects")

    if not os.path.exists('./precomputed_matrix_X.npy'):
        X_precalculated = [[0 for x in range(dim)] for y in range(dim)]

        for i in range(0, dim):
            sum_P = np.sum(specDF.iloc[i].to_numpy())

            if sum_P == 0.0:
                continue

            P = specDF.iloc[i].to_numpy() / sum_P

            for j in range(0, dim):
                if i == j:
                    X_precalculated[i][j] = 1.0
                    continue

                sum_Q = np.sum(specDF.iloc[j].to_numpy())

                if sum_Q == 0.0:
                    X_precalculated[i][j] = 0.0
                    continue

                Q = specDF.iloc[j].to_numpy() / sum_Q

                entro1 = kl_div(Q, P)
                entro1[entro1 == inf] = 0
                distance1 = np.sum(entro1)

                entro2 = kl_div(P, Q)
                entro2[entro2 == inf] = 0
                distance2 = np.sum(entro2)

                distance = (distance1 + distance2) / 2
                X_precalculated[i][j] = np.abs(np.abs(distance) - 1)

        X_precalculated = np.asarray(X_precalculated)
        np.save("precomputed_matrix_X.npy", X_precalculated)
    else:
        X_precalculated = np.load('./precomputed_matrix_X.npy')

    X_precalculated = X_precalculated.T + X_precalculated
    np.fill_diagonal(X_precalculated, np.ones(dim))

    # print("Precalculated matrix using KL divergence: " + str(X_precalculated))
    print("Mean distance: " + str(np.mean(X_precalculated)))
    print("Max distance: " + str(np.max(X_precalculated)))
    print("Min distance: " + str(np.min(X_precalculated)))

    X_precalculated = X_precalculated.tolist()

    # n_neighbors = 5 as kneighbors function returns distance of point to itself (i.e. first column will be zeros)
    nbrs = NearestNeighbors(n_neighbors=39, metric="precomputed").fit(X_precalculated)
    # Find the k-neighbors of a point
    neigh_dist, neigh_ind = nbrs.kneighbors(X_precalculated)

    # sort the neighbor distances (lengths to points) in ascending order
    # axis = 0 represents sort along first axis i.e. sort along row
    sort_neigh_dist = np.sort(neigh_dist, axis=0)
    k_dist = sort_neigh_dist[:, 38]
    # plt.plot(k_dist)
    # # 0.09 Extended Dataset
    # # 0.07 Takami Dataset
    # plt.axhline(y=0.001, linewidth=1, linestyle='dashed', color='k')
    # plt.ylabel("k-NN distance")
    # plt.xlabel("Sorted observations 38th NN)")
    # plt.show()

    plt.figure()
    plt.hist(np.array(X_precalculated).flatten(), bins=24, ec="black", histtype="bar", range=[0.0, 8.0], color='pink')
    ax = plt.gca()
    ax.set_yscale('log')
    plt.xlabel("Similarity")
    plt.ylabel("Count")
    plt.show()

    # clusters = DBSCAN(eps=0.0005, min_samples=39, metric="precomputed").fit(X_precalculated)
    clusters = HDBSCAN(metric="precomputed").fit(X_precalculated)
    print(clusters.labels_)
    print(set(clusters.labels_))
    print(Counter(clusters.labels_))
    print(len(clusters.labels_))

    indices_of_first = [i for i in range(len(clusters.labels_)) if clusters.labels_[i] == 0]
    indices_of_second = [i for i in range(len(clusters.labels_)) if clusters.labels_[i] == 1]
    indices_of_third = [i for i in range(len(clusters.labels_)) if clusters.labels_[i] == 2]
    indices_of_fourth = [i for i in range(len(clusters.labels_)) if clusters.labels_[i] == 3]
    indices_of_noise = [i for i in range(len(clusters.labels_)) if clusters.labels_[i] == -1]

    if not os.path.exists("./results/0"):
        os.mkdir("./results/0")
    if not os.path.exists("./results/1"):
        os.mkdir("./results/1")
    if not os.path.exists("./results/2"):
        os.mkdir("./results/2")
    if not os.path.exists("./results/3"):
        os.mkdir("./results/3")
    if not os.path.exists("./results/-1"):
        os.mkdir("./results/-1")

    print("Timestamps (rows) belonging to noise cluster: ")
    for index in indices_of_noise:
        # print(specDF.iloc[index])
        plt.plot(range(0, len(specDF.iloc[index])), specDF.iloc[index].to_numpy())
        plt.savefig("./results/-1/" + str(specDF.iloc[index].name).replace(":", "-"))
        plt.clf()

    print("Timestamps (rows) belonging to 0 cluster: ")
    for index in indices_of_first:
        # print(specDF.iloc[index])
        plt.plot(range(0, len(specDF.iloc[index])), specDF.iloc[index].to_numpy())
        plt.savefig("./results/0/" + str(specDF.iloc[index].name).replace(":", "-"))
        plt.clf()

    print("Timestamps (rows) belonging to 1 cluster: ")
    for index in indices_of_second:
        # print(specDF.iloc[index])
        plt.plot(range(0, len(specDF.iloc[index])), specDF.iloc[index].to_numpy())
        plt.savefig("./results/1/" + str(specDF.iloc[index].name).replace(":", "-"))
        plt.clf()

    print("Timestamps (rows) belonging to 2 cluster: ")
    for index in indices_of_third:
        # print(specDF.iloc[index])
        plt.plot(range(0, len(specDF.iloc[index])), specDF.iloc[index].to_numpy())
        plt.savefig("./results/2/" + str(specDF.iloc[index].name).replace(":", "-"))
        plt.clf()

    print("Timestamps (rows) belonging to 3 cluster: ")
    for index in indices_of_fourth:
        # print(specDF.iloc[index])
        plt.plot(range(0, len(specDF.iloc[index])), specDF.iloc[index].to_numpy())
        plt.savefig("./results/3/" + str(specDF.iloc[index].name).replace(":", "-"))
        plt.clf()

    specDF['Clusters'] = pd.Series(clusters.labels_)
    plotX = pd.DataFrame(np.array(specDF)).fillna(0)
    print(specDF.columns)
    plotX.columns = specDF.columns
    print(plotX.columns)

    pca_1d = PCA(n_components=1)
    pca_2d = PCA(n_components=2)
    pca_3d = PCA(n_components=3)

    PCs_1d = pd.DataFrame(pca_1d.fit_transform(plotX.copy().drop(["Clusters"], axis=1)))
    PCs_2d = pd.DataFrame(pca_2d.fit_transform(plotX.copy().drop(["Clusters"], axis=1)))
    PCs_3d = pd.DataFrame(pca_3d.fit_transform(plotX.copy().drop(["Clusters"], axis=1)))

    PCs_1d.columns = ["PC1_1d"]
    PCs_2d.columns = ["PC1_2d", "PC2_2d"]
    PCs_3d.columns = ["PC1_3d", "PC2_3d", "PC3_3d"]

    plotX = pd.concat([plotX, PCs_1d, PCs_2d, PCs_3d], axis=1, join='inner')

    cluster0 = plotX[plotX["Clusters"] == 0]
    cluster1 = plotX[plotX["Clusters"] == 1]
    cluster2 = plotX[plotX["Clusters"] == 2]

    trace1 = go.Scatter3d(
        x=cluster0["PC1_3d"],
        y=cluster0["PC2_3d"],
        z=cluster0["PC3_3d"],
        mode="markers",
        name="Cluster 0",
        marker=dict(color='rgba(255, 128, 255, 0.8)'),
        text=None)

    # trace2 is for 'Cluster 1'
    trace2 = go.Scatter3d(
        x=cluster1["PC1_3d"],
        y=cluster1["PC2_3d"],
        z=cluster1["PC3_3d"],
        mode="markers",
        name="Cluster 1",
        marker=dict(color='rgba(255, 128, 2, 0.8)'),
        text=None)

    # trace3 is for 'Cluster 2'
    trace3 = go.Scatter3d(
        x=cluster2["PC1_3d"],
        y=cluster2["PC2_3d"],
        z=cluster2["PC3_3d"],
        mode="markers",
        name="Cluster 2",
        marker=dict(color='rgba(0, 255, 200, 0.8)'),
        text=None)

    data = [trace1, trace2, trace3]

    # title = "Visualizing Clusters in Three Dimensions Using PCA"
    # layout = dict(title=title,
    #               xaxis=dict(title='PC1', ticklen=5, zeroline=False),
    #               yaxis=dict(title='PC2', ticklen=5, zeroline=False)
    #               )
    # fig = dict(data=data, layout=layout)

    fig = go.Figure(data=data)
    fig.show()


if __name__ == "__main__":
    main()
