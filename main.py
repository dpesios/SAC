import numpy as np
import pandas
from tabulate import tabulate
from datetime import datetime, timedelta

pandas.set_option('display.max_columns', None)

filename_list = ['./data/dsc_fc_summed_spectra_2016_v01.csv', './data/dsc_fc_summed_spectra_2017_v01.csv', \
                 './data/dsc_fc_summed_spectra_2018_v01.csv', './data/dsc_fc_summed_spectra_2019_v01.csv', \
                 './data/dsc_fc_summed_spectra_2020_v01.csv', './data/dsc_fc_summed_spectra_2021_v01.csv', \
                 './data/dsc_fc_summed_spectra_2022_v01.csv', './data/dsc_fc_summed_spectra_2023_v01.csv']

kp_file_name = './data/OMNI2_H0_MRG1HR_81346.csv1'

def loadDataset():
    dataframes_list = []
    for file_name in filename_list:
        file_dataDF = pandas.read_csv(file_name, delimiter=',', parse_dates=[0], infer_datetime_format=True, na_values='0', header=None)
        dataframes_list.append(file_dataDF)

    datasetDF = pandas.concat(dataframes_list)

    col_names = [str(i) for i in range(0, len(datasetDF.columns))]
    col_names[0] = 'Time'
    col_names[1] = 'B_x'
    col_names[2] = 'B_y'
    col_names[3] = 'B_z'

    datasetDF.columns = col_names

    # Convert timestamps to datetime indices
    datasetDF['Time'] = datasetDF['Time'].astype(str)
    datasetDF['Time'] = pandas.to_datetime(datasetDF['Time'])
    datasetDF = datasetDF.set_index('Time')

    return datasetDF


def loadKpData():
    kp_dataDF = pandas.read_csv(kp_file_name, delimiter=',', parse_dates=True, infer_datetime_format=True, na_values='0',
                                  skiprows=76, skipfooter=3, header="infer")

    # Properly format the timestamps
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].map(
        lambda x: x.split('.')[0].replace("T", " ")
    )

    # Convert timestamps to datetime indices
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].astype(str)
    kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'] = pandas.to_datetime(kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'])

    # Reindex to be able to upsample the time series
    start_time = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].iloc[0]
    end_time = kp_dataDF['EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ'].iloc[-1]
    t_index = pandas.DatetimeIndex(pandas.date_range(start=start_time-timedelta(seconds=30.0), end=end_time, freq="30S"))

    kp_dataDF = kp_dataDF.set_index('EPOCH_BIN_yyyy-mm-ddThh:mm:ss.sssZ')
    kp_dataDF = kp_dataDF.reindex(t_index)

    kp_dataDF = kp_dataDF.resample('30S', kind='timestamp').asfreq()[0:len(kp_dataDF)]

    # Fill NaN values with the average of adjacent values
    kp_dataDF['3-H_KP*10_'] = (kp_dataDF['3-H_KP*10_'].ffill() + kp_dataDF['3-H_KP*10_'].bfill()) / 2
    kp_dataDF['3-H_KP*10_'] = kp_dataDF['3-H_KP*10_'].bfill().ffill()

    kp_dataDF.index.name = 'Time'
    kp_dataDF = kp_dataDF.drop(['3-H_KP*10_CNTS', '3-H_KP*10_CNTS.1', '#_OF_3-H_KP*10_CNTS'], axis=1)

    return kp_dataDF


if __name__ == '__main__':
    datasetDF = loadDataset()
    print("Length 1st: " + str(len(datasetDF)))
    print(tabulate(datasetDF.tail(20), headers='keys', tablefmt='psql'))
    # print(tabulate(datasetDF.tail(20), headers='keys', tablefmt='psql'))

    kp_dataDF = loadKpData()
    print("Length 2nd: " + str(len(kp_dataDF)))
    print(tabulate(kp_dataDF.head(20), headers='keys', tablefmt='psql'))
    # print(tabulate(kp_dataDF.tail(20), headers='keys', tablefmt='psql'))
    # print(tabulate(kp_dataDF.sample(n=20, random_state=1), headers='keys', tablefmt='psql'))

    overall_dataDF = pandas.merge(datasetDF, kp_dataDF, on="Time")
    print("Length 3rd: " + str(len(overall_dataDF)))
    print(tabulate(overall_dataDF.sample(n=30, random_state=1), headers='keys', tablefmt='psql'))
    print(tabulate(overall_dataDF.tail(20), headers='keys', tablefmt='psql'))

    # filtered_dataDF = overall_dataDF.loc['2023-04-10 00:00:00':'2023-04-13 00:00:00']
    # print("Length 4th: " + str(len(filtered_dataDF)))
    # print(tabulate(filtered_dataDF.tail(20), headers='keys', tablefmt='psql'))

    # It should produce only 24 rows, since we declared three hours
    overall_per_3hour_dataDF = overall_dataDF.rolling(window=3*60, step=3*60).mean()
    print("Length 5th: " + str(len(overall_per_3hour_dataDF)))
    print(tabulate(overall_per_3hour_dataDF.tail(20), headers='keys', tablefmt='psql'))
